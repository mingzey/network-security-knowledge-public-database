
```
@echo off
setlocal ENABLEDELAYEDEXPANSION

::初始数据
set /a ii=0 
for /l %%i in (1,1,100) do ( 

::TODO
ping -n 1 -w 5 192-168-1-!ii!.pvp.docker.com

set /a ii+=1
)
```

## 原理
靶机提供的地址如果是前面带有以下特征的，X是目标靶机的位置，未知
192-168-1-X.pvp.docker.com

由于这个网址需要被DNS解析，所以可以通过遍历扫描能否正常返回对应的IP地址即可
无论发包是否得到相应