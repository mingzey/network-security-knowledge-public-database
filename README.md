# (必读)网络安全知识共享库
您可以在本库阅读、共享您的网络安全知识
- 本库基于`Obsidian`打造
- **不建议在 Git 网站上直接阅读，有些 Obsidian 的高级内容和语法无法渲染**
- **不建议在 Git 网站上直接阅读，有些 Obsidian 的高级内容和语法无法渲染**
- **不建议在 Git 网站上直接阅读，有些 Obsidian 的高级内容和语法无法渲染**
- 建议使用 Git 进行 Clone 后使用 `Obsidian` 进行阅读
- 建议每次阅读前使用 `git pull` 同步这个共享库，保障你的内容都是最新的

## 为什么使用`Obsidian`?
众所周知，或者你有所不知，关于网络安全方面的知识非常广泛，通常一个知识点涉及非常多的的知识，而Obsidian特有的**双向链接**特性，使得这些知识能够被快速溯源，相互连接，形成完整的知识链接。并且 Obsidian 支持知识点的可视化，这使得 Obsidian 可以成为你的**第二大脑**。

**相信我，使用 Obsidian 会给你带来前所未有的知识浏览体验**

关于Obsidian的好处请见：[玩转 Obsidian 01：打造知识循环利器](https://sspai.com/post/62414)

## 如何使用
### 直接使用
* 下载本仓库并解压到文件夹
* 下载安装 Obsidian
* 使用 Obsidian 打开解压到的文件夹

### 专业使用
* 下载 Git [x32](https://www.onlinedown.net/soft/1182431.htm) [x64](https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=1&rsv_idx=1&tn=baidu&wd=git%2064%E4%BD%8D&fenlei=256&oq=git%252064%25E4%25BD%258D%2520%25E5%25B7%25A5%25E5%2585%25B7&rsv_pq=a010153b000a3545&rsv_t=541dhTuQlz7bmiJ4flhHhSn0ddcshWlpFGomTY8cu0HLyGliAP3Rf4P35Yk&rqlang=cn&rsv_enter=0&rsv_dl=tb&rsv_btype=t&inputT=1124&rsv_sug3=16&rsv_sug1=10&rsv_sug7=000&rsv_sug4=1362&rsv_sug=1) [学习GIT](https://learngitbranching.js.org/?locale=zh_CN)
* 使用 Git Pull 本库 (--rebase origin *)
* 下载 [Obsidian](https://obsidian.md/)
* 打开本库所在的文件夹

### 在安卓上使用
* 下载Termux(可选，安装Git)
* 下载Obsidian
* 打开本库根目录


## 如何贡献 
* 将本库同步
* Fork本库
* 做出你自己的修改
* Pull Request 我们审核
* 进行合并

2021/10/9 MingZeY